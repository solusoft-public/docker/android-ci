# syntax=docker/dockerfile:1
FROM openjdk:18-jdk-alpine3.14
LABEL description="Android docker for CI"
LABEL maintainer="info@solusoft.es"

# install OS packages
RUN apk update --quiet
RUN apk add --quiet wget tar unzip xxd "ruby>2.7.4"

RUN gem update --system --no-document
RUN gem install bundler --force

# Setup environment
ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/platform-tools

# Install Android SDK
ADD https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip downloaded_sdk.zip
RUN mkdir -p /opt/android-sdk-linux/cmdline-tools
RUN unzip downloaded_sdk.zip -d /opt/android-sdk-linux/cmdline-tools
RUN rm -f downloaded_sdk.zip
RUN mv /opt/android-sdk-linux/cmdline-tools/cmdline-tools /opt/android-sdk-linux/cmdline-tools/latest

# Install sdk elements (list from "sdkmanager --list")
RUN sdkmanager "cmdline-tools;3.0"
RUN sdkmanager "platform-tools"
RUN sdkmanager "patcher;v4"

# Updating everything again
RUN sdkmanager --update

# accepting licenses
RUN yes | sdkmanager --licenses
