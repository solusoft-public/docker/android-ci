# Android CI

Imagen de docker base con lo necesario para CI/CD en projectos Android.

## Dependencias y elementos preinstalados

- wge, tar, unzip, lib32stdc++6, lib32z1, build-essential y xxd
- ruby-install: https://github.com/postmodern/ruby-install
- ruby 2.7.4 y bundler
- Android Command Line Tools, Platform Tools (generico) y Patcher (v4).

## Contributing & License

solusoft  

Todos los derechos reservados. Consultar [www.solusoft.es](http://www.solusoft.es/contactar.aspx)  